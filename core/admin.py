from django.contrib import admin

from .models import Platform, Game

# Register your models here.
admin.site.register(Platform)
admin.site.register(Game)
