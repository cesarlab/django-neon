from django.urls import path

from .views import index, dynamic_lookup_view

urlpatterns = [
    path('', index, name='index'),
    path('games/<int:id>/', dynamic_lookup_view, name='game-detail')
]
