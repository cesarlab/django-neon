from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404

from .models import Game

# Create your views here.
def index(request):
    context = {
        'queryset': Game.objects.all()
    }

    return render(request, 'core/index.html', context)

def dynamic_lookup_view(request, id):
    context = {
        'object': get_object_or_404(Game, id=id)
    }

    return render(request, 'core/dynamic_view.html', context)
