from django.db import models
from django.urls import reverse
from django.db.models import ImageField

# Create your models here.
class Platform(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Game(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=1000)
    platform = models.ForeignKey(Platform, on_delete=models.SET_NULL, null=True, blank=True)
    cover = ImageField(upload_to='static/upload/game_cover', null=True, blank=True, height_field=None, width_field=None, max_length=100)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('game-detail', args=[str(self.id)])

    class Meta:
        ordering = ['title']
