# Django Neon Integration

Project created with the purpose of learning to connect a web project developed with [Django](https://www.djangoproject.com/) framework to a remote __PostgreSQL__ database on [Neon](https://neon.tech/home).

## How to run
- Clone this repository with `git clone https://gitlab.com/cesarlab/django-neon.git`
- Move to project folder `cd django-neon`
- Create virtual environment `virtualenv venv-name`
- Access Virtual Environment `source/venv-name/bin/activate`
- Create .env file:
    SECRET_KEY='random-secret-key'
    DEBUG=True # change to False before in production
- Migrate database `python manage.py makemigrations`, `python manage.py migrate`
- Create super user `python manage.py createsuperuser`
- Start development server `python manage.py runserver`
- Open project at `localhost:8000` on web browser
- You can access the admin area at `localhost:8000/admin` with your super user credentials

a demo version is available through this [link](https://neonpostgre.pythonanywhere.com)

this project is under MIT License.
